import { Injectable } from '@angular/core';
import { Workbook } from 'exceljs';

import * as fs from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor() {

  }

  public generateExcel(mappedJson) {
    const header = ['Inquiry #', 'Client Name', '	Activity Type', 'Follow Up Date', 'Activity Date', 'Start Time', 'Completed Date', 'Action Type', 'Caller Name', 'Caller Phone', 'Details', 'Status', 'Created By', 'Assigned To']
    const workbook = new Workbook();
    const worksheet = workbook.addWorksheet('Activity');
    const headerRow = worksheet.addRow(header)

    // Cell Style : Fill and Border
    headerRow.eachCell((cell, number) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '969696' },
        bgColor: { argb: '969696' },
      };
      cell.font = { bold: true,size:13 };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });

      let finalarr = [];
      for(let i=0;i<mappedJson.length;i++){
         finalarr[i]=[
         mappedJson[i].recordId ?? '',
         mappedJson[i].clientName ?? '',
         mappedJson[i].activityType ?? '',
         mappedJson[i].followUpDate ?? '',
         mappedJson[i].activityDate ?? '',
         mappedJson[i].startTime ?? '',
         mappedJson[i].completedDate ?? '',
         mappedJson[i].type ?? '',
         mappedJson[i].callerName ?? '',
         mappedJson[i].callerPhone ?? '',
         mappedJson[i].details ?? '',
         mappedJson[i].status ?? '',
         mappedJson[i].createdBy ?? '',
         mappedJson[i].assignedTo ?? ''
        ]
        worksheet.addRow(finalarr[i]);
      }
      console.log(finalarr);


    worksheet.getColumn(1).width = 10
    worksheet.getColumn(2).width = 30
    worksheet.getColumn(3).width = 25;
    worksheet.getColumn(4).width = 25;
    worksheet.getColumn(5).width = 25;
    worksheet.getColumn(6).width = 25;
    worksheet.getColumn(7).width = 25;
    worksheet.getColumn(8).width = 20;
    worksheet.getColumn(9).width = 20;
    worksheet.getColumn(10).width = 20;
    worksheet.getColumn(11).width = 60;
    worksheet.getColumn(12).width = 20;
    worksheet.getColumn(13).width = 20;
    worksheet.getColumn(14).width = 40;
    worksheet.getColumn(15).width = 40;
    worksheet.getColumn(14).alignment={wrapText:true};
    worksheet.getColumn(11).alignment={wrapText:true};


    // // worksheet.addRow([]);




    // Generate Excel File with given name
    workbook.xlsx.writeBuffer().then((data: any) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, 'Activites.xlsx');
    });

  }


}
