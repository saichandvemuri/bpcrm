import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { forkJoin, Observable, throwError } from 'rxjs';
import { catchError, switchAll } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { LogDataComponent } from '../core/log-data/log-data.component';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  public referralPrivilageId = null;
  public credentialingprivilageId = null;
  public popupRef: DynamicDialogRef;
  public userResponse: any;
  public headerName: string = null;
  public lookupresponse: any;
  private _apiurl: string = "https://jsonplaceholder.typicode.com/posts";
  constructor(private http: HttpClient,public dialogService: DialogService) {
    console.log('api service')
    this.userResponse = JSON.parse(sessionStorage.getItem('loggedInUser'))

    if (this.userResponse !== undefined && this.userResponse != null) {
      if (this.userResponse.isAdmin == 1) {
        this.referralPrivilageId = 1;
        this.credentialingprivilageId = 1;
      } else {
        this.userResponse.privilegedArray.map(x => {
          if (x.objectName == 'credentialing') {
            this.credentialingprivilageId = x.privilegeId;
          }
          if (x.objectName == "referral") {
            this.referralPrivilageId = x.privilegeId;
          }
        })
      }
    }

  }
  public SetheaderName(value: string) {
    this.headerName = value;
  }

  public getlookup() {
    this.getLookupsData().subscribe(res => {
      this.lookupresponse = res;
    })

  }
  public getInquiresLokkup() {
    this.getInquires(JSON.stringify({ "userId": this.userResponse.userId, "lowerBound": 0, "upperBound": 0 })).subscribe(res => {
      this.inquiriesLookUp = res.inquiryList;
    })
  }
  getdata(): Observable<any> {
    return this.http.get<any>(`${this._apiurl}`);
  }
  public inquiriesLookUp = [];


  public authenticateUser(jsondata: string): Observable<any> {
    return this.http.post("/ssoapi/authenticateUser", jsondata, httpOptions).pipe(catchError(this.errorHandler));
  }
  public changePassword(jsondata): Observable<any> {
    return this.http.post("/ssoapi/changePassword", jsondata, httpOptions).pipe(catchError(this.errorHandler));
  }

  public getRole(id): Observable<any> {
    return this.http.get(`/securityapi/getRole?id=${id}`).pipe(catchError(this.errorHandler));
  }
  public createRole(obj): Observable<any> {
    return this.http.post(`/securityapi/createRole`, obj, httpOptions).pipe(catchError(this.errorHandler));
  }
  public getRolesList(obj): Observable<any> {
    return this.http.get(`/securityapi/getRolesList?role=${obj}`).pipe(catchError(this.errorHandler))
  }
  public createGroup(obj): Observable<any> {
    return this.http.post(`/securityapi/createGroup`, obj, httpOptions).pipe(catchError(this.errorHandler));
  }
  public GetGroupsList(obj): Observable<any> {
    return this.http.get(`/securityapi/getGroupsList?jsonObj=${obj}`).pipe(catchError(this.errorHandler))
  }
  public getGroup(id) {
    return this.http.get(`/securityapi/getGroup?id=${id}`).pipe(catchError(this.errorHandler))
  }

  public getLookupsData(): Observable<any> {
    return this.http.get(`/common/getLookupsData?lookupNames=program,activity_status,funding_category,referral_category,relationship_to_client,activity_type,non_conversion_reason,hold_reason,state,status,gender,branch,users,requested_services,user_type`).pipe(catchError(this.errorHandler))
  }
  public getInquiryList(obj): Observable<any> {
    return this.http.post('/referral/getInquiryList',obj,httpOptions).pipe(catchError(this.errorHandler));
  }
  public getInquires(obj): Observable<any> {
    return this.http.get(`/referral/getInquiries?jsonObj=${obj}`).pipe(catchError(this.errorHandler));

  }
  public getInquiryDetails(obj): Observable<any> {
    return this.http.get(`/referral/getInquiryDetails?jsonObj=${obj}`).pipe(catchError(this.errorHandler));
  }

  public saveInquiry(obj): Observable<any> {
    return this.http.post(`/referral/saveInquiry`, obj, httpOptions).pipe(catchError(this.errorHandler));
  }

  public deleteInquiry(obj): Observable<any> {
    return this.http.get(`/referral/deleteInquiry?jsonObj=${obj}`).pipe(catchError(this.errorHandler));
  }

  public saveBranch(obj): Observable<any> {
    return this.http.post(`/referral/saveBranch`, obj, httpOptions).pipe(catchError(this.errorHandler));

  }
  public getBranchList(obj): Observable<any> {
    return this.http.post('/referral/getBranchList',obj,httpOptions).pipe(catchError(this.errorHandler));
  }
  public getBranchDetails(id): Observable<any> {
    return this.http.get(`/referral/getBranchDetails?jsonObj={branchId:${id}}`).pipe(catchError(this.errorHandler));
  }

  public getActivityList(obj): Observable<any> {
    return this.http.post('/referral/getActivityList',obj,httpOptions).pipe(catchError(this.errorHandler));
  }
  public getInquiryLogDetails(obj): Observable<any> {
    return this.http.get(`/referral/getInquiryLogDetails?jsonObj=${obj}`).pipe(catchError(this.errorHandler));
  }

  public getActivityDetails(obj): Observable<any> {
    return this.http.get(`/referral/getActivityDetails?jsonObj=${obj}`).pipe(catchError(this.errorHandler));
  }
  public saveActivity(obj): Observable<any> {
    return this.http.post(`/referral/saveActivity`, obj, httpOptions).pipe(catchError(this.errorHandler));

  }
  public deleteActivity(obj): Observable<any> {
    return this.http.get(`/referral/deleteActivity?jsonObj=${obj}`).pipe(catchError(this.errorHandler));
  }
  public getlookupdateOptional(str): Observable<any> {
    return this.http.get(`/common/getLookupsData?lookupNames=${str}`);
  }
public getUserDetails(id): Observable<any>{
 return this.http.get(`/ssoapi/getUserDetails?jsonObj={"id":${id}}`).pipe(catchError(this.errorHandler));
}
public getUserList(obj): Observable<any>{
  return this.http.post('/ssoapi/getUserList',obj,httpOptions).pipe(catchError(this.errorHandler));
 }
public reSetPassword(obj): Observable<any>{
  return this.http.post(`/ssoapi/reSetPassword`,obj,httpOptions).pipe(catchError(this.errorHandler));
 }
 public unlockUser(obj): Observable<any>{
  return this.http.post(`/ssoapi/unlockUser`,obj,httpOptions).pipe(catchError(this.errorHandler));
 }
 public saveUser(obj): Observable<any>{
  return this.http.post(`/ssoapi/saveUser`,obj,httpOptions).pipe(catchError(this.errorHandler));
 }

 public getAdhocQueryAvailableFields():Observable<any>{
   return this.http.get(`/adhoc/getAdhocQueryAvailableFields?jsonObj={"userId":${this.userResponse.userId},"tableName":"inquiry"}`).pipe(catchError(this.errorHandler));
 }
 public saveAdhocQueryDetails(postJson):Observable<any>{
  return this.http.post(`/adhoc/saveAdhocQueryDetails`,postJson,httpOptions).pipe(catchError(this.errorHandler));
}
 public getSavedAdHocQueryList():Observable<any>{
   return this.http.get(`/adhoc/getSavedAdHocQueryList?jsonObj={"userId":${this.userResponse.userId}}`).pipe(catchError(this.errorHandler));
 }
 public getAdhocQueryDetailsById(reportId):Observable<any>{
   return this.http.get(`/adhoc/getAdhocQueryDetailsById?jsonObj={"userId":${this.userResponse.userId},"reportId":${reportId}}}`);
 }
 public runAdhocQuery(reportId):Observable<any>{
   return this.http.get(`/adhoc/runAdhocQuery?jsonObj={"userId":${this.userResponse.userId},"reportId":${reportId}}`).pipe(catchError(this.errorHandler));
 }
 public deleteAdhocQuery(reportId):Observable<any>{
   return this.http.get(`/adhoc/deleteAdhocQuery?jsonObj={"userId":${this.userResponse.userId},"reportId":${reportId}}`).pipe(catchError(this.errorHandler));
 }

 public copyActivities(postJson):Observable<any>{
   return this.http.post('/referral/copyActivities',postJson,httpOptions).pipe(catchError(this.errorHandler));
 }

 public activityDownload(reqArr:Array<any>): Observable<any[]> {
  let response=[];
  for(let i=0;i<reqArr.length;i++){
    response.push(this.getActivityList(reqArr[i]));
  }

  // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6;

  return forkJoin(response);
}



  //log dialog

  public ref: DynamicDialogRef;
  public  showLogdata(item) {
      this.ref = this.dialogService.open(LogDataComponent, {
          header: 'Log Data ',
          width: '70%',
          contentStyle: {"max-height": "90vh", "overflow": "auto"},
          baseZIndex: 10000,
          styleClass:"log-data-dialog",
          data:item
      });

  }


  private errorHandler(error: HttpErrorResponse) {
    console.log("error in API service", error);
    Swal.fire({
      title: error.status,
      html: error.message,
      icon: 'error'
    })
    return throwError(error);
  }

  public loggedIn(): boolean {
    return !!sessionStorage.getItem('loggedInUser');
  }
}

export const httpOptions = {
  headers: new HttpHeaders({
    // 'Content-Type': 'application/json'
    'Content-Type': 'text/plain',

  }),

};
