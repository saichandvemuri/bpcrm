import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ApiService } from 'src/app/service/api.service';

@Component({
  selector: 'app-log-data',
  templateUrl: './log-data.component.html',
  styleUrls: ['./log-data.component.scss']
})
export class LogDataComponent implements OnInit {

public popupData=null;

  public logList=[];


  constructor(public apiService:ApiService, public config: DynamicDialogConfig,public ref: DynamicDialogRef) {
         console.log(this.config)
         this.popupData=this.config.data;

   }

  ngOnInit(): void {
    this.getInquiryLogDetails();
  }
public getInquiryLogDetails(){

  try {
    let obj={userId:this.apiService.userResponse.userid,recordId:this.popupData?.recordId}
    this.apiService.getInquiryLogDetails(JSON.stringify(obj)).subscribe(res=>{
       this.logList=res.inquiryLogList;
    })
  } catch (error) {

  }
}

}
