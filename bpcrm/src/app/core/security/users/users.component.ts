import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';
import { Location } from '@angular/common';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public userList = [];
  public totalReocrdCount = 0;
  public userForm:FormGroup;
  public formError=false;
  public lowerBound=1;
  public upperBound=20;
  public password="";
  public resetPassword=null;
  public activeFilter=2;
  public loginIdFilter='';
  public userNameFilter='';
  public currentLoginId=null;
  tabIndex: number = 0;
  public dialogVisible: boolean = false;
  public edit: boolean = false;
  selectedValue;
  public checked: boolean = false;
  public checked1: boolean = false;
  public privilage: boolean = false;
  public list2 = [];
  public displayBasic: boolean = false;
  constructor(public _apiservice: ApiService,private _fb:FormBuilder,private loaction: Location) {
    if(this._apiservice.userResponse.isAdmin==0){
     this.loaction.back();
    }else{
      this._apiservice.SetheaderName('Users');
      this.getUserList();
    }
  }
  ngOnInit(): void {
    this.createForm();

  }

  // showDialog() {
  //   this.dialogVisible = true;
  // }
  // editRole() {
  //   this.edit = true;
  //   console.log(this.edit)
  // }
  // next() {
  //   this.tabIndex = 1;
  // }
  // prev() {
  //   this.tabIndex = 0;
  // }
  public passwordPopup(item) {
    this.currentLoginId=item.loginId;
    this.displayBasic = true;
  }

 public getUserList(filter?){
   try {
     filter!=undefined?filter.hide():'';
     let obj={"loginId":this.loginIdFilter,"active":this.activeFilter,"userName":this.userNameFilter,"lowerBound":this.lowerBound,"upperBound":this.upperBound}
     this._apiservice.getUserList(JSON.stringify(obj)).subscribe(res=>{
       console.log(res);
       this.userList=res.userList;
       this.totalReocrdCount=res.totalRecordsCount;

     })
   } catch (error) {

   }
 }
 public getUserDetails(id){
   this.formError=false;
   try {
     this._apiservice.getUserDetails(id).subscribe(res=>{
       console.log(res)
       this.userForm=this._fb.group({
        loginId:new FormControl(res.loginId,[Validators.required, NameValidator.noWhiteSpace]),
        isAdmin:new FormControl(res.admin==0?false:true),
        password:new FormControl("abc",Validators.required),
        reTypePassword:new FormControl("abc",Validators.required),
        lastName:new FormControl(res.lastName,[Validators.required, NameValidator.noWhiteSpace]),
        middleName:new FormControl(res.middleName),
        firstName:new FormControl(res.firstName,[Validators.required, NameValidator.noWhiteSpace]),
        active:new FormControl(res.active==1?true:false),
        email:new FormControl(res.email??null,Validators.required),
        userType:new FormControl(res?.userTypeId==0?null:res?.userTypeId,Validators.required),

        id:new FormControl(res.id)
     })
     })
   } catch (error) {

   }
 }
 get formControls(){
  return this.userForm.controls;
}

 public createForm(){
   this.formError=false;
   this.userForm=this._fb.group({
      loginId:new FormControl('',[Validators.required, NameValidator.noWhiteSpace]),
      isAdmin:new FormControl(false),
      password:new FormControl('',Validators.required),
      reTypePassword:new FormControl('',Validators.required),
      lastName:new FormControl('',[Validators.required, NameValidator.noWhiteSpace]),
      middleName:new FormControl(''),
      firstName:new FormControl('',[Validators.required, NameValidator.noWhiteSpace]),
      email:new FormControl(null,[Validators.required]),
      active:new FormControl(true),
      userType:new FormControl(null,Validators.required),
      id:new FormControl(0)
   })
 }
 public saveUser(){
 this.formError=true;
  if(this.userForm.valid){
      if(this.userForm.value.password!=this.userForm.value.reTypePassword){
        Swal.fire('Invalid', 'New Password and Re Type Password should  match', 'warning')

      }else{
       let emailValidator=/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
       if(emailValidator.test(this.userForm.value.email)){
        try {
          let obj=
          {"loginId":this.userForm.value.loginId.trim(),"userId":this._apiservice.userResponse.userId,"password":this.userForm.value.password,"isAdmin":this.userForm.value.isAdmin==true?1:0,"lastName":this.userForm.value.lastName.trim(),"middleName":this.userForm.value.middleName==null?'':this.userForm.value.middleName,
          "firstName":this.userForm.value.firstName.trim(),"id":this.userForm.value.id,"active":this.userForm.value.active==true?1:0,'email':this.userForm.value.email??'',"userTypeId":this.userForm.value.userType??0}
          this._apiservice.saveUser(obj).subscribe(res=>{
            console.log(res)
            if(res.successFlag==0){
             Swal.fire('',res.message,'error');
            }else{
              Swal.fire('',res.message,'success');
              this._apiservice.getlookupdateOptional('users').subscribe(lookupRes=>{
                console.log(lookupRes);
                this._apiservice.lookupresponse.users=lookupRes.users;
              })
              this.getUserList();
              this.createForm();
            }
          })
        } catch (error) {

        }
      }else{
        Swal.fire('Invalid', 'Please Enter valid Email', 'warning')

      }
    }
  }else{
      Swal.fire('Invalid', 'Please fill all mandatory fields', 'warning')

    }

 }
 public updateUser(){
   this.formError=true;

  if(this.userForm.valid){

     if(this.userForm.value.password!=this.userForm.value.reTypePassword){
        Swal.fire('Invalid', 'New Password and Confirm Password should  match', 'warning')

      }else{
        try {
          let obj=
          {"loginId":this.userForm.value.loginId.trim(),"userId":this._apiservice.userResponse.userId,"password":"","isAdmin":this.userForm.value.isAdmin==true?1:0,"lastName":this.userForm.value.lastName.trim(),"middleName":this.userForm.value.middleName==null?'':this.userForm.value.middleName,
          "firstName":this.userForm.value.firstName.trim(),"id":this.userForm.value.id,"active":this.userForm.value.active==true?1:0,email:this.userForm.value.email??'',"userTypeId":this.userForm.value.userType??0}
          this._apiservice.saveUser(obj).subscribe(res=>{
            console.log(res);
            if(res.successFlag==0){
              Swal.fire('',res.message,'error');
             }else{
               Swal.fire('',res.message,'success');
               this.getUserList();
               this.createForm();

             }
          })
        } catch (error) {

        }
      }
    }else{
      Swal.fire('Invalid', 'Please fill all mandatory fields', 'warning')

    }

 }
 public unlockUser(obj){
   try {
     let jsonObj= {"loginId":obj.loginId,"userId":this._apiservice.userResponse.userId}
     this._apiservice.unlockUser(JSON.stringify(jsonObj)).subscribe(res=>{
      if(res.successFlag==0){
        Swal.fire('',res.message,'error');
      }else{
        Swal.fire('',res.message,'success');
        this.getUserList()
      }

     })
   } catch (error) {

   }
 }

 public reSetPassword(){
   try {
     let obj={"loginId":this.currentLoginId,"userId":this._apiservice.userResponse.userId,"password":this.password,"resetPassword":0}
     this._apiservice.reSetPassword(obj).subscribe(res=>{
       console.log(res);
       if(res.successFlag==0){
        Swal.fire('',res.message,'error');
       }else{
         Swal.fire('',res.message,'success');
         this.displayBasic=false;
         this.currentLoginId=null;
         this.password=null;

       }

     })

   } catch (error) {

   }
 }

 public onPageChange(event){
   console.log(event)
   this.lowerBound = (event.page) * event.rows + 1;
   this.upperBound = this.lowerBound + event.rows - 1;
   console.log(this.lowerBound,this.upperBound)
   this.getUserList();
  }




}
export class NameValidator {
  static noWhiteSpace(control: AbstractControl) : ValidationErrors | null {
    // console.log(control.value.trim());
    let val=control.value.trim();
    if(val.length==0){
      return {noWhiteSpace: true}

    }
      return null;
  }
}
