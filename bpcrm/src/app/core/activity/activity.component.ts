import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Paginator } from 'primeng/paginator';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';
import { DialogService } from 'primeng/dynamicdialog';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { CreateInquiryComponent } from '../inquiry/create-inquiry/create-inquiry.component';
import { OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ExcelService } from 'src/app/service/excel.service';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit, OnDestroy {
  @ViewChild('p', { static: false }) paginator: Paginator;

  public recordId = null;
  // public pagination=false;
  public branchId = null;
  public activityList = [];
  public callerName = null;
  public phoneNumber = null;
  // public lookupresponse:any;
  public totalRecordsCount = 0;
  public activeDateSort = true;
  public startTimeSort = true;
  public completedDateSort = true;
  public followUpSort = true;
  public activityTypeId = null;
  public createdBy = this._apiservice.userResponse.userId;

  public lowerbound = 1;
  public upperBound = 10;
  public clientName = '';
  public orderBy = 'activity date';
  public orderType = "asc";
  public detailsPopup: boolean = false;
  public detailsData = '';
  public filterData: any;
  public filterForm: FormGroup;

  constructor(public _apiservice: ApiService, public excelService: ExcelService, private router: Router, private activatedRoute: ActivatedRoute, public dialogService: DialogService, private _fb: FormBuilder, public datePipe: DatePipe) {
    sessionStorage.removeItem('inquiryId');
    sessionStorage.removeItem('clientName');
    this._apiservice.getInquiresLokkup();
    this._apiservice.SetheaderName('Activities');

    let filterData = JSON.parse(sessionStorage.getItem('activityFilter'));
    if (filterData != null) {
      // this.recordId=filterData.recordId!=0?filterData.recordId:null;
      // this.clientName=filterData.clientName;
      // this.callerName=filterData.callerName;
      // this.phoneNumber=filterData.callerPhone;
      // this.activityTypeId=filterData.activityTypeId!=0?filterData.activityTypeId:null;
      // console.log(this.activityTypeId)
      console.log(filterData);
      this.filterForm = this._fb.group({
        inquiryNumber: new FormControl(filterData.recordId != 0 ? filterData.recordId : null),
        clientName: new FormControl(filterData.clientName),
        callerName: new FormControl(filterData.callerName),
        callerPhone: new FormControl(filterData.callerPhone),
        createdById: new FormControl(filterData.createdById),
        activityTypeId: new FormControl(filterData.activityTypeId != 0 ? filterData.activityTypeId : null),
        followUpDate: new FormControl(filterData.followUpDate),

      })
      this.activeDateSort = filterData.activeDateSort;
      this.startTimeSort = filterData.startTimeSort;
      this.completedDateSort = filterData.completedDateSort;
      this.followUpSort = filterData.followUpSort;
      this.orderBy = filterData.orderBy;
      this.orderType = filterData.orderType;
    } else {
      this.createForm();
    }
  }
  ngOnInit(): void {
    this.getActivityList();
  }

  public createForm() {
    this.filterForm = this._fb.group({
      inquiryNumber: new FormControl(null),
      clientName: new FormControl(''),
      callerName: new FormControl(null),
      callerPhone: new FormControl(null),
      createdById: new FormControl(this._apiservice.userResponse.userId),
      activityTypeId: new FormControl(null),
      followUpDate: new FormControl(null)

    })
  }
  public getActivityList() {
    try {
      let obj = {
        "orderBy": this.orderBy, callerName: this.filterForm.value.callerName == null ? '' : this.filterForm.value.callerName.trim(), callerPhone: this.filterForm.value.callerPhone == null ? '' : this.filterForm.value.callerPhone,
        "order": this.orderType, "recordId": this.filterForm.value.inquiryNumber == null ? 0 : this.filterForm.value.inquiryNumber, clientName: this.filterForm.value.clientName.trim(), lowerBound: this.lowerbound, upperBound: this.upperBound,
        "activityTypeId": this.filterForm.value.activityTypeId == null ? 0 : this.filterForm.value.activityTypeId, "createdBy": this.filterForm.value.createdById == null ? 0 : this.filterForm.value.createdById, followUpDate: this.filterForm.value.followUpDate != null ? this.datePipe.transform(this.filterForm.value.followUpDate, 'MM/dd/yyyy') ?? '' : ''
      }
      this._apiservice.getActivityList(JSON.stringify(obj)).subscribe(res => {
        console.log(res)
        this.activityList = res.activityList;
        this.totalRecordsCount = res.totalRecordsCount;
        console.log(this.activityList);
      })

    } catch (error) {

    }

  }


  public onGo(event?) {
    event ? this.paginator.changePageToFirst(event) : '';
    this.getActivityList();


  }

  public onsort(ordertype, orderby, event) {
    this.orderType = ordertype;
    this.orderBy = orderby;
    this.paginator.changePageToFirst(event);
    this.getActivityList();
  }
  public onPageChange(event) {
    console.log(event.rows);


    this.lowerbound = (event.page) * event.rows + 1;
    this.upperBound = this.lowerbound + event.rows - 1;

    this.getActivityList();

    console.log(event)
  }
  public addActivity() {
    this.router.navigateByUrl(`/create-activity`)



  }

  public deleteActivity(id) {
    Swal.fire({
      title: 'Are you sure you want to Delete?',
      // text: "you want to Logout?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes, Delete'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          let obj = { "userId": this._apiservice.userResponse.userId, recordId: id }
          this._apiservice.deleteActivity(JSON.stringify(obj)).subscribe(res => {
            console.log(res);
            Swal.fire('', res.message, 'success');
            //  this.getInquiryList();
            this.getActivityList();

          })

        } catch (error) {

        }
      }
    })
  }

  public editActivity(recordId, id) {
    console.log("ediy actuvity")
    sessionStorage.setItem('activityId', id);
    this.router.navigateByUrl(`/create-activity`)
    // this.router.navigateByUrl(`/create-activity/${recordId}/${+id}`)
  }


  ref: DynamicDialogRef;

  public activitypopup(id) {
    this._apiservice.popupRef = this.dialogService.open(CreateInquiryComponent, {
      header: 'Inquiry Details',
      width: '96%',
      contentStyle: { "overflow": "auto" },
      baseZIndex: 10000,
      styleClass: 'activityedit',
      data: { inquiryId: id }
    });

  }

  public ngOnDestroy() {
    if (this.ref) {
      this.ref.close();
    }
    console.log("destroy")
    sessionStorage.removeItem('inquiryId')
    sessionStorage.removeItem('clientName');

    this.filterData = {
      callerName: this.filterForm.value.callerName == null ? '' : this.filterForm.value.callerName,
      callerPhone: this.filterForm.value.callerPhone == null ? '' : this.filterForm.value.callerPhone,
      clientName: this.filterForm.value.clientName == null ? '' : this.filterForm.value.clientName,
      recordId: this.filterForm.value.inquiryNumber,
      activityTypeId: this.filterForm.value.activityTypeId,
      createdById: this.filterForm.value.createdById,
      followUpDate: this.filterForm.value.followUpDate != null ? this.datePipe.transform(this.filterForm.value.followUpDate, 'MM/dd/yyyy') : '',
      activeDateSort: this.activeDateSort,
      startTimeSort: this.startTimeSort,
      completedDateSort: this.completedDateSort,
      followUpSort: this.followUpSort,
      orderBy: this.orderBy,
      orderType: this.orderType
    }
    sessionStorage.setItem('activityFilter', JSON.stringify(this.filterData));
    //close log data
    if (this._apiservice.ref) {
      this._apiservice.ref.close();
    }
  }

  public onDetailClick(data) {
    this.detailsPopup = true;
    this.detailsData = data;
  }




  public downoloadMultipleRequest() {

    let downloadLowerBound = 1;
    let downloadUpperBound = 20000;
    let reqArr = [];
    let resArr = [];


    for (let i = 0; i < this.totalRecordsCount; i += 20000) {
      console.log(i);
      let obj = {
        "orderBy": this.orderBy, callerName: this.filterForm.value.callerName == null ? '' : this.filterForm.value.callerName.trim(), callerPhone: this.filterForm.value.callerPhone == null ? '' : this.filterForm.value.callerPhone,
        "order": this.orderType, "recordId": this.filterForm.value.inquiryNumber == null ? 0 : this.filterForm.value.inquiryNumber, clientName: this.filterForm.value.clientName.trim(), lowerBound: downloadLowerBound + i, upperBound: downloadUpperBound + i,
        "activityTypeId": this.filterForm.value.activityTypeId == null ? 0 : this.filterForm.value.activityTypeId,
        "createdBy": this.filterForm.value.createdById == null ? 0 : this.filterForm.value.createdById, followUpDate: this.filterForm.value.followUpDate != null ? this.datePipe.transform(this.filterForm.value.followUpDate, 'MM/dd/yyyy') ?? '' : ''
      };

      console.log(obj);
      // if(i<100000){
      reqArr.push(JSON.stringify(obj))
      // }
    }
    this._apiservice.activityDownload(reqArr).subscribe(res => {

      for (let i = 0; i < res.length; i++) {
        console.log(res[i].activityList)
        resArr = [...resArr, ...res[i].activityList];
        console.log(resArr);
      }
      console.log(resArr);
      this.excelService.generateExcel(resArr);
    })
  }



  exportexcel(mappedJson: Array<any>): void {
    let finalarr = [];
    for(let i=0;i<mappedJson.length;i++){
       finalarr[i]=[
       mappedJson[i].recordId ?? '',
       mappedJson[i].clientName ?? '',
       mappedJson[i].activityType ?? '',
       mappedJson[i].followUpDate ?? '',
       mappedJson[i].activityDate ?? '',
       mappedJson[i].startTime ?? '',
       mappedJson[i].completedDate ?? '',
       mappedJson[i].type ?? '',
       mappedJson[i].callerName ?? '',
       mappedJson[i].callerPhone ?? '',
       mappedJson[i].details ?? '',
       mappedJson[i].status ?? '',
       mappedJson[i].createdBy ?? '',
       mappedJson[i].assignedTo ?? '']
    }
    console.log(finalarr);
    this.excelService.generateExcel(finalarr);
  }



}
