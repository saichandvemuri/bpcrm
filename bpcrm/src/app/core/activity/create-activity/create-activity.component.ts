import { DatePipe } from '@angular/common';
import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-create-activity',
  templateUrl: './create-activity.component.html',
  styleUrls: ['./create-activity.component.scss']
})
export class CreateActivityComponent implements OnInit, OnDestroy {
  public activityForm: FormGroup;
  public inquiryId = null;
  // public lookupResponse:any;
  public formError = false;
  public activityId = 0;
  constructor(public router: Router, public _apiservice: ApiService, private date: DatePipe, private _fb: FormBuilder, private activatedRoute: ActivatedRoute) {
    this.createForm();
    if (sessionStorage.getItem('inquiryId') != undefined) {
      this.inquiryId = +sessionStorage.getItem('inquiryId');

      this.onInquirySelect({ value: this.inquiryId });
      let name = sessionStorage.getItem('clientName') != undefined ? sessionStorage.getItem('clientName') : ''
      this.activityForm.get('inquiryClientName').setValue(name);

    }

    if (sessionStorage.getItem('activityId') != undefined) {
      this.activityId = +(sessionStorage.getItem('activityId'));
      console.log(this.activityId)
      this.getActivityDetails(this.activityId)
      this._apiservice.SetheaderName('Edit Activity');
    } else {
    this._apiservice.getInquiresLokkup();
    this._apiservice.SetheaderName('Add Activity');

      if (this._apiservice.referralPrivilageId == 1 || this._apiservice.referralPrivilageId == 2) {
        console.log("staying in activity")
      } else {

        this.router.navigateByUrl('/Activity')
      }

    }

  }
  ngOnInit(): void {
    // this.getLookups();
  }
  public createForm() {
    this.formError = false;
    this.activityForm = this._fb.group({
      activityId: new FormControl(0),
      inquiryClientName: new FormControl(null),
      activityTypeId: new FormControl(null),
      hold: new FormControl(null),
      nonConversionReasonId: new FormControl(null),
      activityDate: new FormControl(null),
      detail: new FormControl(''),
      assignedTo: new FormControl(null),
      createdBy: new FormControl(this._apiservice.userResponse?.userId),
      startTime: new FormControl(null),
      inquiryId: new FormControl(this.inquiryId, Validators.required),
      followUpDate: new FormControl(),
      status:new FormControl(1),

    })
  }
  get formControls() {
    return this.activityForm.controls;
  }

  public saveActivity() {
    this.formError = true;
    console.log(this.activityForm.value, this.activityForm.valid)
    let errStr = '';
    if (this.activityForm.valid) {
      if (this.activityForm.value.activityDate == null && this.activityForm.value.followUpDate != null) {
        errStr += 'Activity date is mandatory when follow up date is filled. <br>'
      } if (this.activityForm.value.activityTypeId == 7 && (this.activityForm.value.followUpDate == null || this.activityForm.value.followUpDate == undefined)) {
        errStr = 'Follow Up Date is required when Activity Type is "To Do". <br> '
      }
      if (this.activityForm.value.activityDate != null && this.activityForm.value.followUpDate != null) {
        let activityDate = Date.parse(this.date.transform(this.activityForm.value.activityDate, 'MM/dd/yyyy'));
        let followUpDate = Date.parse(this.date.transform(this.activityForm.value.followUpDate, 'MM/dd/yyyy'));

        if (followUpDate < activityDate) {
         errStr+'Follow up date  cannot be greater than activity date <br>';

        }

      }

      if (errStr.length==0) {
        try {


          let obj =
          {
            "userId": this._apiservice.userResponse.userId,
            "inquiryId": this.activityForm.value.inquiryId,
            "activityId": this.activityForm.value.activityId,
            "activityTypeId": this.activityForm.value.activityTypeId == null ? 0 : this.activityForm.value.activityTypeId,
            "holdReasonId": this.activityForm.value.hold == null || this.activityForm.value.activityTypeId != 4 ? 0 : this.activityForm.value.hold,
            "nonConversionReasonId": this.activityForm.value.nonConversionReasonId == null || this.activityForm.value.activityTypeId != 6 ? 0 : this.activityForm.value.nonConversionReasonId,
            "activityDate": this.activityForm.value.activityDate != null ? this.date.transform(this.activityForm.value.activityDate, 'MM/dd/yyyy') : '',
            "details": this.activityForm.value.detail,
            "assignedTo": this.activityForm.value.assignedTo == null ? 0 : this.activityForm.value.assignedTo,
            "createdBy": this.activityForm.value.createdBy == null ? 0 : this.activityForm.value.createdBy,
            "startTime": this.activityForm.value.startTime ?? '',
            "followUpDate": this.activityForm.value.followUpDate != null ? this.date.transform(this.activityForm.value.followUpDate, 'MM/dd/yyyy') : '',
            "activityStatusId":this.activityForm.value.status

          }
          console.log(obj)
          this._apiservice.saveActivity(JSON.stringify(obj)).subscribe(res => {
            console.log(res)
            Swal.fire('', res.message, 'success');
            this._apiservice.SetheaderName('Add Activity');
            sessionStorage.removeItem('activityId')
            this.router.navigateByUrl('activity');

            // this.createForm();
          })
        } catch (error) {

        }
      }else{
        Swal.fire('Invalid',errStr,'warning');
      }
     }
      else {
        Swal.fire('Invalid', 'Please fill all mandatory fields', 'warning')

      }
    }

  public getActivityDetails(id) {
    let obj = { "activityId": id }
    try {
      this._apiservice.getActivityDetails(JSON.stringify(obj)).subscribe(res => {
        console.log(res);
        this.editForm(res);
      })
    } catch (error) {

    }
  }
  public editForm(obj) {
    this.activityForm = this._fb.group({
      activityId: new FormControl(obj.activityId),
      inquiryClientName: new FormControl(obj.inquiryClientName),
      activityTypeId: new FormControl(obj.activityTypeId),
      hold: new FormControl(obj.holdReasonId),
      nonConversionReasonId: new FormControl(obj.nonConversionReasonId),
      activityDate: new FormControl(obj?.activityDate),
      detail: new FormControl(obj.details ? obj.details : ''),
      assignedTo: new FormControl(obj.assignedTo),
      createdBy: new FormControl(obj.createdBy),
      inquiryId: new FormControl(obj.recordId),
      startTime: new FormControl(obj.startTime),
      followUpDate: new FormControl(obj?.followUpDate),
      status:new FormControl(obj?.activityStatusId)
    })
  }


  public onActivityDateChange(event) {
    if (event == undefined) {
      this.activityForm.get('startTime').setValue(null);
    } else {
      this.activityForm.get('startTime').setValue(event);

    }
    console.log(event)


  }
  public onInquirySelect(event) {
    this.activityForm.get('inquiryId').setValue(event.value)
    console.log(event.value)
    for (let i = 0; i < this._apiservice.inquiriesLookUp.length; i++) {
      if (this._apiservice.inquiriesLookUp[i]?.recordId == event.value) {
        this.activityForm.get('inquiryClientName').setValue(this._apiservice.inquiriesLookUp[i]?.clientName)

      }
    }
  }
  public ngOnDestroy() {
    //   console.log("destroy")
    //  if()
    //  sessionStorage.removeItem('inquiryId')
    //  sessionStorage.removeItem('clientName');
    sessionStorage.removeItem('activityId')

  }
}
