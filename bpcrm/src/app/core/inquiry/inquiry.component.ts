import { DatePipe } from '@angular/common';
import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Paginator } from 'primeng/paginator';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { PrimeNGConfig } from 'primeng/api';
@Component({
  selector: 'app-inquiry',
  templateUrl: './inquiry.component.html',
  styleUrls: ['./inquiry.component.scss']
})
export class InquiryComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('paginator', { static: true }) paginator: Paginator;

  public inquiryFilterForm: FormGroup;

  public inquiryDateSort: boolean = false;
  public dateCreatedSort: boolean = false;
  public branchNameSort: boolean = false;
  public lastUpdatedSort: boolean = false;
  public recordOwnerSort: boolean = false;
  public clientLastNameSort: boolean = false;
  public lastActivityDateSort: boolean = false;

  public filterData: any;
  public currentPage = 0;

  public orderBy = "date created";
  public orderType = "desc";
  public activityList = [];

  // customers = [1];
  public recordId = null;
  public branchId = null;
  public clientName = null;
  public statusId = 1;
  public callerName = null;
  public callerNumber = null;
  public recordOwner = this._apiservice.userResponse.userId;
  private applyrecordOwner = this._apiservice.userResponse.userId;
  private applyRecordId = null;
  private applyInquiryDateStart = null;
  private applyInquiryDateEnd = null;
  private applyBranchId = null;
  private applyClientName = null;
  private applyStatusId = 1;
  private applycallerName = null;
  private applycallerNumber = null;
  public perPage = 10;
  public lowerbound = 1;
  public upperBound = 10;
  public totalRecordsCount = 0;
  // public branchList = [];
  public inquiriesList = [];
  // public statuslist = [];
  public sysDate: Date = new Date();
  public inquiryDateStart: Date = new Date();
  public inquiryDateEnd: Date = new Date();
  public branchName: string;
  public inquiryId = null;

  public branchFilter=null;

  constructor(public _apiservice: ApiService, private router: Router, private _fb: FormBuilder, private datePipe: DatePipe, private primengConfig: PrimeNGConfig) {
    sessionStorage.removeItem('inquiryId');
    sessionStorage.removeItem('clientName');
    this._apiservice.SetheaderName('Inquiries');
    this._apiservice.getInquiresLokkup();

    let filterData = JSON.parse(sessionStorage.getItem('inquiryFilter'));
    console.log(filterData, "flterdata");
    console.log("RecordId", this.recordId);
    console.log(filterData != null)
    if (filterData != null) {

      this.inquiryFilterForm = this._fb.group({
        inquiryNumber: new FormControl(filterData.recordId != 0 ? filterData.recordId : null),
        startDate: new FormControl(new Date(filterData.startDate)),
        endDate: new FormControl(new Date(filterData.endDate)),
        branch: new FormControl(filterData.branch != 0 ? filterData.branch : null),
        clientName: new FormControl(filterData.clientName),
        statusId: new FormControl(filterData.status != 0 ? filterData.status : null),
        callerName: new FormControl(filterData.callerName),
        callerPhone: new FormControl(filterData.callerPhone),
        recordOwner: new FormControl(filterData.recordOwner != 0 ? filterData.recordOwner : null)

      })


      this.orderType = filterData.orderType;
      this.orderBy = filterData.orderBy;
      this.lowerbound = filterData.lowerBound;
      this.upperBound = filterData.upperBound;
      this.totalRecordsCount = filterData.totalRecordsCount;
      this.inquiryDateSort = filterData.inquiryDateSort;
      this.dateCreatedSort = filterData.dateCreatedSort;
      this.branchNameSort = filterData.branchNameSort;
      this.lastUpdatedSort = filterData.lastUpdatedSort;
      this.recordOwnerSort = filterData.recordOwnerSort;
      this.clientLastNameSort = filterData.clientLastNameSort;
      this.lastActivityDateSort = filterData.lastActivityDateSort


      this.applyRecordId = this.inquiryFilterForm.value.inquiryNumber != null ? this.inquiryFilterForm.value.inquiryNumber : 0;
      this.applyInquiryDateStart = this.inquiryFilterForm.value.startDate != null ? this.datePipe.transform(this.inquiryFilterForm.value.startDate, 'MM/dd/yyyy') : '';
      this.applyInquiryDateEnd = this.inquiryFilterForm.value.endDate != null ? this.datePipe.transform(this.inquiryFilterForm.value.endDate, 'MM/dd/yyyy') : '';
      this.applyBranchId = this.inquiryFilterForm.value.branch!= null ? this.inquiryFilterForm.value.branch.id : 0;
      this.applyClientName = this.inquiryFilterForm.value.clientName != null ? this.inquiryFilterForm.value.clientName.trim() : '';
      this.applyStatusId = this.inquiryFilterForm.value.statusId != null ? this.inquiryFilterForm.value.statusId : 0;
      this.applycallerName = this.inquiryFilterForm.value.callerName != null ? this.inquiryFilterForm.value.callerName.trim() : '';
      this.applycallerNumber = this.inquiryFilterForm.value.callerPhone != null ? this.inquiryFilterForm.value.callerPhone.trim() : '';
      this.applyrecordOwner = this.inquiryFilterForm.value.recordOwner != null ? this.inquiryFilterForm.value.recordOwner : 0;

      this.updateCurrentPage(filterData.currentPage)


    }
    else {
      this.inquiryDateStart.setDate(new Date().getDate() - 31);
      this.createForm();

    }


  }
  ngOnInit(): void {
    this.primengConfig.ripple = true;
  }
  ngAfterViewInit() {

    this.onGo();

  }
  public getInquiryList() {
    try {
      let obj = {
        userId: this._apiservice.userResponse.userId, recordId: this.applyRecordId, inquiryDateStart: this.applyInquiryDateStart, callerName: this.applycallerName, callerPhone: this.applycallerNumber,
        inquiryDateEnd: this.applyInquiryDateEnd, branchId: this.applyBranchId, clientName: this.applyClientName, statusId: this.applyStatusId, lowerBound: this.lowerbound, upperBound: this.upperBound,
        "order": this.orderType, "orderBy": this.orderBy, recordOwner: this.applyrecordOwner
      }
      this._apiservice.getInquiryList(JSON.stringify(obj)).subscribe(res => {
        console.log(res);
        this.inquiriesList = res.inquiryList;
        this.totalRecordsCount = res.totalRecordsCount;
      })
    } catch (error) {

    }
  }
  public createForm() {

    this.inquiryFilterForm = this._fb.group({
      inquiryNumber: new FormControl(null),
      startDate: new FormControl(new Date(this.inquiryDateStart)),
      endDate: new FormControl(new Date()),
      branch: new FormControl(null),
      clientName: new FormControl(null),
      statusId: new FormControl(1),
      callerName: new FormControl(null),
      callerPhone: new FormControl(null),
      recordOwner: new FormControl(this._apiservice.userResponse.userId)

    })
  }

  public edit(id, name) {
    sessionStorage.setItem('inquiryId', id);
    sessionStorage.setItem('clientName', name);
    this.router.navigateByUrl(`/create-inquiry`)
  }

  public onGo(event?) {
    console.log(event)
    console.log(this.inquiryDateStart, this.inquiryDateEnd)
    let dateFlag = false;


    if (this.inquiryFilterForm.value.startDate != null && this.inquiryFilterForm.value.endDate != null) {
      let startDate = Date.parse(this.datePipe.transform(this.inquiryFilterForm.value.startDate, 'MM/dd/yyyy'));
      let endDate = Date.parse(this.datePipe.transform(this.inquiryFilterForm.value.endDate, 'MM/dd/yyyy'));
      if (startDate > endDate) {
        Swal.fire(`Invalid date`, 'Start date cannot be greater than End date', 'warning');
      } else {
        dateFlag = true;
      }
    }
    else {
      if (this.inquiryFilterForm.value.startDate == null && this.inquiryFilterForm.value.endDate != null) {
        Swal.fire(`Invalid date`, 'Start date cannot be empty', 'warning');
      } else
        if (this.inquiryFilterForm.value.startDate != null && this.inquiryFilterForm.value.endDate == null) {
          Swal.fire(`Invalid date`, 'End date cannot be empty', 'warning');
        } else if (this.inquiryFilterForm.value.startDate == null && this.inquiryFilterForm.value.endDate == null) {
          Swal.fire(`Invalid date`, 'Start date and End date cannot be empty', 'warning');
        }
    }
    if (dateFlag) {
      this.applyRecordId = this.inquiryFilterForm.value.inquiryNumber != null ? this.inquiryFilterForm.value.inquiryNumber : 0;
      this.applyInquiryDateStart = this.inquiryFilterForm.value.startDate != null ? this.datePipe.transform(this.inquiryFilterForm.value.startDate, 'MM/dd/yyyy') : '';
      this.applyInquiryDateEnd = this.inquiryFilterForm.value.endDate != null ? this.datePipe.transform(this.inquiryFilterForm.value.endDate, 'MM/dd/yyyy') : '';
      this.applyBranchId = this.inquiryFilterForm.value.branch != null ? this.inquiryFilterForm.value.branch.id: 0;
      this.applyClientName = this.inquiryFilterForm.value.clientName != null ? this.inquiryFilterForm.value.clientName.trim() : '';
      this.applyStatusId = this.inquiryFilterForm.value.statusId != null ? this.inquiryFilterForm.value.statusId : 0;
      this.applycallerName = this.inquiryFilterForm.value.callerName != null ? this.inquiryFilterForm.value.callerName.trim() : '';
      this.applycallerNumber = this.inquiryFilterForm.value.callerPhone != null ? this.inquiryFilterForm.value.callerPhone.trim() : '';
      this.applyrecordOwner = this.inquiryFilterForm.value.recordOwner != null ? this.inquiryFilterForm.value.recordOwner : 0;
      //---------------------------------------------------------------------------
      // this.applyRecordId = this.recordId != null ? this.recordId : 0;
      // this.applyInquiryDateStart = this.inquiryDateStart != null ? this.datePipe.transform(this.inquiryDateStart, 'MM/dd/yyyy') : '';
      // this.applyInquiryDateEnd = this.inquiryDateEnd != null ? this.datePipe.transform(this.inquiryDateEnd, 'MM/dd/yyyy') : '';
      // this.applyBranchId = this.branchId != null ? this.branchId : 0;
      // this.applyClientName = this.clientName != null ? this.clientName.trim() : '';
      // this.applyStatusId = this.statusId != null ? this.statusId : 0;
      // this.applycallerName = this.callerName != null ? this.callerName.trim() : '';
      // this.applycallerNumber = this.callerNumber != null ? this.callerNumber.trim() : '';
      // this.applyrecordOwner=this.recordOwner!=null?this.recordOwner:0;
      event ? this.paginator.changePageToFirst(event) : '';
      this.getInquiryList();
      // this.reset = 0;
    }
  }


  public onPageChange(event) {
    console.log(event)
    console.log(this.perPage);
    this.currentPage = event.page;


    this.lowerbound = (event.page) * event.rows + 1;
    this.upperBound = this.lowerbound + event.rows - 1;
    this.perPage = event.rows;

    this.getInquiryList();

    console.log(event)
  }
  public deleteInquiry(id) {
    Swal.fire({
      title: 'Are you sure you want to Delete?',
      // text: "you want to Logout?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Yes, Delete'
    }).then((result) => {
      if (result.isConfirmed) {

        try {
          let obj = { "userId": this._apiservice.userResponse.userId, recordId: id }
          this._apiservice.deleteInquiry(JSON.stringify(obj)).subscribe(res => {
            console.log(res);
            Swal.fire('', res.message, 'success');
            this.getInquiryList();

          })

        } catch (error) {

        }
      }
    })




  }

  public activityRoute(id, name) {
    sessionStorage.setItem('inquiryId', id);
    console.log(name, name != 'undefined', name == 'undefined')
    // let data=name!='undefined'?name:''
    sessionStorage.setItem('clientName', name);

    this.router.navigateByUrl(`/activity`);
    // this.router.navigateByUrl(`/activity/${id}/${name}`);
  }



  exportexcel2(): void {
    let mappedJson = [];

    let obj = {
      userId: this._apiservice.userResponse.userId, recordId: this.applyRecordId, inquiryDateStart: this.applyInquiryDateStart, callerName: this.applycallerName, callerPhone: this.applycallerNumber,
      inquiryDateEnd: this.applyInquiryDateEnd, branchId: this.applyBranchId, clientName: this.applyClientName, statusId: this.applyStatusId, lowerBound: 1, upperBound: this.totalRecordsCount,
      "order": this.orderType, "orderBy": this.orderBy,
    }
    this._apiservice.getInquiryList(JSON.stringify(obj)).subscribe(res => {
      console.log(res);
      let inquiriesList = res.inquiryList;
      // this.totalRecordsCount = res.totalRecordsCount;

      mappedJson = inquiriesList.map(item => {
        return {
          "Inquiry #": item?.recordId,
          "Inquiry Date": item?.inquiryDate,
          "Date Created": item?.dateCreated,
          "Branch Name": item?.branchName,
          "Client Name": item?.clientName,
          "Caller Name	": item?.callerName,
          "Caller Phone": item?.callerPhone,
          "Status": item?.status,
          "# Activities": item?.activities,
          "Last Activity Date": item?.inquiryDate,
          "Follow-up Date": item?.followUpDate,
          "Record Owner": item?.recordOwner,
          "Last Modified By": item?.lastModifiedBy,
          "SOC Date": item?.socDate,
          "Conversion": item?.conversion,
          "Last Updated": item?.lastUpdated
        }
      });
      var wscols = [
        { wch: 10 },
        { wch: 15 },
        { wch: 25 },
        { wch: 25 },
        { wch: 20 },
        { wch: 15 },
        { wch: 25 },
        { wch: 15 },
        { wch: 10 },
        { wch: 25 },
        { wch: 25 },
        { wch: 15 },
        { wch: 25 },
        { wch: 25 },
        { wch: 25 },
        { wch: 25 },


      ];
      const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(mappedJson);
      worksheet["!cols"] = wscols
      const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });


      // /* table id is passed over here */
      // let element = document.getElementById('excel-table');
      // const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

      // /* generate workbook and add the worksheet */
      // const wb: XLSX.WorkBook = XLSX.utils.book_new();
      // XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

      // /* save to file */
      let name = 'Inquiry';
      this.saveAsExcelFile(excelBuffer, name);

      // XLSX.writeFile(wb, name);


    })



  }
  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: 'string' });
    /***********`
    *YOUR EXCEL FILE'S NAME
    */
    FileSaver.saveAs(data, fileName + '.xlsx');
  }

  ngOnDestroy() {
    console.log("ondestroy")

    this.filterData = {
      recordId: this.applyRecordId,
      startDate: this.applyInquiryDateStart,
      endDate: this.applyInquiryDateEnd,
      branch: this.inquiryFilterForm.value.branch,
      clientName: this.applyClientName,
      status: this.applyStatusId,
      callerName: this.applycallerName,
      callerPhone: this.applycallerNumber,
      orderBy: this.orderBy,
      orderType: this.orderType,
      lowerBound: this.lowerbound,
      upperBound: this.upperBound,
      currentPage: this.currentPage,
      totalRecordsCount: this.totalRecordsCount,
      recordOwner: this.applyrecordOwner,
      inquiryDateSort: this.inquiryDateSort,
      dateCreatedSort: this.dateCreatedSort,
      branchNameSort: this.branchNameSort,
      lastUpdatedSort: this.lastUpdatedSort,
      recordOwnerSort: this.recordOwnerSort,
      clientLastNameSort: this.clientLastNameSort,
      lastActivityDateSor: this.lastActivityDateSort
    }
    sessionStorage.setItem('inquiryFilter', JSON.stringify(this.filterData));
    if (this._apiservice.ref) {
      this._apiservice.ref.close();
    }
  }

  public onsort(ordertype, orderby, event) {
    this.orderType = ordertype;
    this.orderBy = orderby;
    this.paginator.changePageToFirst(event);
    this.getInquiryList();
  }

  public onDateChange(event, flag) {
    console.log("event" + flag, event);

    if (event != undefined) {
      if (flag == "start") {
        this.inquiryDateStart = event;
      } else {
        this.inquiryDateEnd = event;
      }

    }
  }

  // public branchChange(event) {
  //   console.log(event)
  //   if (event != null) {
  //     // this.branchId = event.id;
  //     this.inquiryFilterForm.get('branchId').setValue(event);
  //     this.branchName = event.name;

  //   } else {
  //     this.branchId = null;
  //     // this.inquiryFilterForm.get('branchId').setValue(null);
  //     this.branchName = null;
  //   }


  // }
  private updateCurrentPage(currentPage: number): void {
    console.log(currentPage)
    setTimeout(() => this.paginator.changePage(+currentPage));
  }

  public logData(item): void {
    let json = { recordId: item.recordId, dateCreated: item.dateCreated, recordOwner: item.recordOwner }
    this._apiservice.showLogdata(json)
  }
  // ngOnDestroy() {
  //   if (this.ref) {
  //       this.ref.close();
  //   }
  // }
  displayModal: boolean = false;
  public copyactivity(id) {
    this.newInquiryId = id;
    this.inquiryId = null;
    this.activityCheckedAllFlag = false;
    this.activityList = [];
    this.displayModal = true;
  }

  public activityCheckedAllFlag = false;
  public newInquiryId = null;
  public selectedActivities = [];
  public getActivityList(id) {
    try {
      if (id != null) {
        this.activityList = [];
        this.activityCheckedAllFlag = false;
        let obj = { "recordId": id == null ? 0 : id, clientName: '', lowerBound: 0, upperBound: 0 }
        this._apiservice.getActivityList(JSON.stringify(obj)).subscribe(res => {
          console.log(res)
          this.activityList = res.activityList;

          this.totalRecordsCount = res.totalRecordsCount;
        })
      }
      else {
        this.activityList = [];

      }
    } catch (error) {

    }

  }
  public onSellecAll() {
    console.log(this.activityCheckedAllFlag)
    if (this.activityCheckedAllFlag) {
      this.selectedActivities = this.activityList.map(x => {
        return x.activityId
      })
    } else {
      this.selectedActivities = [];
    }
    console.log(this.selectedActivities)

  }

  public copyActivites() {
    try {
      let obj = { userId: this._apiservice.userResponse.userId, oldInquiryId: this.inquiryId, newInquiryId: this.newInquiryId, activityIds: this.selectedActivities.toString() }
      this._apiservice.copyActivities(obj).subscribe(res => {
        console.log(res);
        Swal.fire('Success', res.message, 'success');
        this.displayModal = false;
        this.getInquiryList();

      })
    } catch (error) {

    }
  }

}


