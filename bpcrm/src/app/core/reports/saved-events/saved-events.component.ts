import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApiService } from 'src/app/service/api.service';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import { Location } from '@angular/common';
import { Router } from '@angular/router';


@Component({
  selector: 'app-saved-events',
  templateUrl: './saved-events.component.html',
  styleUrls: ['./saved-events.component.scss']
})
export class SavedEventsComponent implements OnInit,OnDestroy {

  public headerColumns = [];
  public inquiryList = [];
  public reportId = 0;
  public fileName='';

  constructor(public _apiservice: ApiService,public location:Location,private router:Router) {
    let id = sessionStorage.getItem('reportId');
    this.fileName=sessionStorage.getItem('fileName');
    this._apiservice.SetheaderName(this.fileName);

    if (id != undefined) {
      this.reportId = +id;
    }else{
      this.router.navigateByUrl('/saved-queries')
    }

  }

  ngOnInit(): void {
    this.runAdhocQuery();
  }

  public runAdhocQuery() {
    try {
      this._apiservice.runAdhocQuery(this.reportId).subscribe(res => {
        console.log(res);
        this.headerColumns = res.headerColumns.split(',');
        this.inquiryList = res.inquiryList;
        this.reportId = res.reportId;
      })
    } catch (error) {

    }
  }

  public exportexcel(): void {
    /* table id is passed over here */
    let element = document.getElementById('excel-table');
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb,`${this.fileName}.xlsx`);

  }

  public onClose(){
        // this.location.back();
        this.router.navigateByUrl('/saved-queries')

  }
public   ngOnDestroy() {
  console.log("on destroy saved events")
 sessionStorage.removeItem('reportId');
  sessionStorage.removeItem('fileName');
}
}

