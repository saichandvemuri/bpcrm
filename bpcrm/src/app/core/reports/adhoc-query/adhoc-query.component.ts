import { DatePipe, Location } from '@angular/common';
import { Component, OnInit, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';
declare var $: any;
@Component({
  selector: 'app-adhoc-query',
  templateUrl: './adhoc-query.component.html',
  styleUrls: ['./adhoc-query.component.scss']
})
export class AdhocQueryComponent implements OnInit {
  @HostListener('window:resize') onResize() {
    this.resize()
  }
  private availableFields = [];

  public inquiryStartDate = null;
  public inquiryEndDate = null;
  public branchIds = [];
  public clientName = null;
  public callerName = null;
  public callerPhone = null;
  public recordOwnerIds = [];
  public statusIds = [];
  public availableFieldsList = [];
  public selectedFields = [];
  public availableSortorderFields = [];
  public selectdSortorderFields = [];
  public reportName = null;
  public comments = null;
  public reportId = 0;
  public lookupResponse: any;
  public localSelectedFilter = [];

  public filterLookups: any =
    [
      { isBlank: 0, isNotblank: 0, elementName: 'bill_post_date', label: 'Bill Post Date', 'elementValue1': '', elementType: 'dateType', value: '10/10/2021-12/12/2021', visible: true, command: (event) => this.onMenuItem(event.item), },
      { isBlank: 0, isNotblank: 0, elementName: 'branch', label: 'Branch', 'elementValue1': '', elementType: 'selectbox', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'client_name', label: 'Client Name', 'elementValue1': '', elementType: 'text', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'conversion_date', label: 'Conversion Date', 'elementValue1': '', elementType: 'dateType', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'funding_category', label: 'Funding Category', 'elementValue1': '', elementType: 'selectbox', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'home_conference', label: 'Home Conference Date', 'elementValue1': '', elementType: 'dateType', visible: true, command: (event) => this.onMenuItem(event.item) },
      // { isBlank: 0, isNotblank: 0, elementName: 'inquiry_date', label: 'Inquiry Date', 'elementValue1': '', elementType: 'dateType', visible: false, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'inquiry_id', label: 'Inquiry #', 'elementValue1': '', elementType: 'selectbox', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'record_owner', label: 'Record Owner', 'elementValue1': '', elementType: 'selectbox', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'soc_date', label: 'SOC Date', 'elementValue1': '', elementType: 'dateType', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'verified_interactant', label: 'Verified Interactant', 'elementValue1': '', elementType: 'radioButton', visible: true, command: (event) => this.onMenuItem(event.item) },
      { isBlank: 0, isNotblank: 0, elementName: 'verified_poc', label: 'Verified POC', 'elementValue1': '', elementType: 'radioButton', visible: true, command: (event) => this.onMenuItem(event.item) },
    ]






  constructor(public _apiservice: ApiService, private datePipe: DatePipe, public location: Location, private router: Router) {
    this.getLookupsData();
    let id = sessionStorage.getItem('reportId');
    if (id != undefined) {
      this._apiservice.SetheaderName('Edit AdHoc Query ');
      this.reportId = +id;
      this.getAdhocQueryDetailsById(id);
      sessionStorage.removeItem('reportId');
    } else {
      this._apiservice.SetheaderName('Create AdHoc Query ');
      this.getAdhocQueryAvailableFields();

    }
    this._apiservice.inquiriesLookUp.length == 0 ? this._apiservice.getInquiresLokkup() : '';

  }

  ngOnInit(): void {
    this.lookupResponse = this._apiservice.lookupresponse;
    this.resize();
  }

  public getAdhocQueryAvailableFields() {

    try {
      this._apiservice.getAdhocQueryAvailableFields().subscribe(res => {
        console.log(res);
        this.availableFields = res.availableFieldsList;
        this.availableFieldsList = res.availableFieldsList;
      })

    } catch (error) {

    }
  }
  public saveAdhocQueryDetails() {
    this.slectedFilters = [...this.localSelectedFilter];
    try {
      let availableFields = this.availableFields.map(x => {
        return x.field;
      })
      let selectedFields = this.selectedFields.map(x => {
        return x.field;
      })
      let selectdSortorderFields = this.selectdSortorderFields.map(x => {
        return x.field;
      })

      let fliterArray = [];

      for (let i = 0; i < this.slectedFilters.length; i++) {
        if (this.slectedFilters[i].elementType == 'selectbox') {
          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            isBlank: this.slectedFilters[i].elementValue1.includes('isBlank') ? 1 : 0,
            isNotBlank: this.slectedFilters[i].elementValue1.includes('isNotBlank') ? 1 : 0,
            elementValue1: this.slectedFilters[i].elementValue1 = this.slectedFilters[i].elementValue1.filter(x => { return Number.isFinite(x) }).toString(),
            elementValue2: '',

          }
          fliterArray.push(obj)
        } else if (this.slectedFilters[i].elementType == 'dateType') {
          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            elementValue1: this.slectedFilters[i].isBlank == 0 && this.slectedFilters[i].isNotBlank == 0 ? this.datePipe.transform(this.slectedFilters[i].elementValue1, 'MM/dd/yyyy') : '',
            elementValue2: this.slectedFilters[i].isBlank == 0 && this.slectedFilters[i].isNotBlank == 0 ? this.datePipe.transform(this.slectedFilters[i].elementValue2, 'MM/dd/yyyy') : '',
            isBlank: this.slectedFilters[i].isBlank,
            isNotBlank: this.slectedFilters[i].isNotBlank,
          }
          fliterArray.push(obj);

        } else if (this.slectedFilters[i].elementType == 'text') {
          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            elementValue1: this.slectedFilters[i].elementValue1??='',
            elementValue2: '',
            isBlank: 0,
            isNotBlank: 0,
          }
          fliterArray.push(obj);
        } else if (this.slectedFilters[i].elementType == 'radioButton') {
          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            elementValue1: this.slectedFilters[i].elementValue1 == false ? 0 : 1,
            elementValue2: '',
            isBlank: 0,
            isNotBlank: 0,
          }
          fliterArray.push(obj);

        }
      }
      let newObj = {
        elementType: 'dateType',
        elementName: "inquiry_date",
        elementValue1: this.datePipe.transform(this.inquiryStartDate, 'MM/dd/yyyy'),
        elementValue2: this.datePipe.transform(this.inquiryEndDate, 'MM/dd/yyyy'),
        isBlank: 0,
        isNotBlank: 0,
      }
      fliterArray.push(newObj)



      let obj = {
        "reportId": this.reportId,
        "userId": this._apiservice.userResponse.userId,
        "reportFilterList": fliterArray,
        "availableFields": availableFields.toString(),
        "selectedFields": selectedFields.toString(),
        "availableSortorderFields": selectedFields.toString(),
        "selectdSortorderFields": selectdSortorderFields.toString(),
        "reportName": this.reportName,
        "comments": this.comments == null ? '' : this.comments
      }


      this._apiservice.saveAdhocQueryDetails(obj).subscribe(res => {
        console.log(res);
        sessionStorage.setItem('fileName', this.reportName);
        sessionStorage.setItem('reportId', res.reportId);
        this.router.navigateByUrl('/saved-events')
      })
    } catch (error) {

    }
  }

  public getAdhocQueryDetailsById(id) {
    try {
      this._apiservice.getAdhocQueryDetailsById(id).subscribe(res => {
        console.log(res);
        this.comments = res.comments;
        this.reportName = res.reportName;
        this.localSelectedFilter = res.reportFieldsList
        for (let i = 0; i < this.localSelectedFilter.length; i++) {
          if (this.localSelectedFilter[i].elementName == "inquiry_date") {
            this.inquiryStartDate = new Date(this.localSelectedFilter[i].elementValue1);
            this.inquiryEndDate = new Date(this.localSelectedFilter[i].elementValue2);
            this.localSelectedFilter[i].elementType = "";
            this.localSelectedFilter[i].elementName = "";

          } else if (this.localSelectedFilter[i].elementType == "selectbox") {
            if (this.localSelectedFilter[i].elementName == 'branch') {
              this.localSelectedFilter[i].label = 'Branch';
            } else if (this.localSelectedFilter[i].elementName == 'record_owner') {
              this.localSelectedFilter[i].label = 'Record Owner';
            } else if (this.localSelectedFilter[i].elementName == 'funding_category') {
              this.localSelectedFilter[i].label = 'Funding Category';
            } else if (this.localSelectedFilter[i].elementName == 'inquiry_id') {
              this.localSelectedFilter[i].label = 'Inquiry #';
            }
            if (this.localSelectedFilter[i].elementValue1 == undefined) {
              this.localSelectedFilter[i].elementValue1 = [];
              if (this.localSelectedFilter[i].isBlank == "1") { this.localSelectedFilter[i].elementValue1.push('isBlank') };
              if (this.localSelectedFilter[i].isNotBlank == "1") { this.localSelectedFilter[i].elementValue1.push('isNotBlank') };

            } else {
              this.localSelectedFilter[i].elementValue1 = this.localSelectedFilter[i].elementValue1.split(',').map(x => +x);
              if (this.localSelectedFilter[i].isBlank == "1") { this.localSelectedFilter[i].elementValue1.push('isBlank') };
              if (this.localSelectedFilter[i].isNotBlank == "1") { this.localSelectedFilter[i].elementValue1.push('isNotBlank') };
            }


            console.log(this.localSelectedFilter[i].elementValue1)
          } else if (this.localSelectedFilter[i].elementType == "dateType" && this.localSelectedFilter[i].elementName != "inquiry_date") {
            if (this.localSelectedFilter[i].elementName == 'home_conference') {
              this.localSelectedFilter[i].label = 'Home Conference Date';
              if (this.localSelectedFilter[i].isBlank == "0" && this.localSelectedFilter[i].isNotBlank == "0") {
                this.localSelectedFilter[i].value = this.localSelectedFilter[i].elementValue1 + '-' + this.localSelectedFilter[i].elementValue2;
                this.localSelectedFilter[i].selectedValue = 'between';
              } else if (this.localSelectedFilter[i].isBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Blank';
              } else if (this.localSelectedFilter[i].isNotBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Not Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Not Blank';
              }

            }
            else if (this.localSelectedFilter[i].elementName == 'conversion_date') {
              this.localSelectedFilter[i].label = 'Conversion Date';
              if (this.localSelectedFilter[i].isBlank == "0" && this.localSelectedFilter[i].isNotBlank == "0") {
                this.localSelectedFilter[i].value = this.localSelectedFilter[i].elementValue1 + '-' + this.localSelectedFilter[i].elementValue2;
                this.localSelectedFilter[i].selectedValue = 'between';
              } else if (this.localSelectedFilter[i].isBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Blank';
              } else if (this.localSelectedFilter[i].isNotBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Not Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Not Blank';
              }

            }
            else if (this.localSelectedFilter[i].elementName == 'soc_date') {
              this.localSelectedFilter[i].label = 'SOC Date';
              if (this.localSelectedFilter[i].isBlank == "0" && this.localSelectedFilter[i].isNotBlank == "0") {
                this.localSelectedFilter[i].value = this.localSelectedFilter[i].elementValue1 + '-' + this.localSelectedFilter[i].elementValue2;
                this.localSelectedFilter[i].selectedValue = 'between';
              } else if (this.localSelectedFilter[i].isBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Blank';
              } else if (this.localSelectedFilter[i].isNotBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Not Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Not Blank';
              }
            } else if (this.localSelectedFilter[i].elementName == 'bill_post_date') {
              this.localSelectedFilter[i].label = 'Bill Post Date';
              if (this.localSelectedFilter[i].isBlank == "0" && this.localSelectedFilter[i].isNotBlank == "0") {
                this.localSelectedFilter[i].value = this.localSelectedFilter[i].elementValue1 + '-' + this.localSelectedFilter[i].elementValue2;
                this.localSelectedFilter[i].selectedValue = 'between';
              } else if (this.localSelectedFilter[i].isBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Blank';
              } else if (this.localSelectedFilter[i].isNotBlank == "1") {
                this.localSelectedFilter[i].value = 'Is Not Blank';
                this.localSelectedFilter[i].selectedValue = 'Is Not Blank';
              }
            }

          } else if (this.localSelectedFilter[i].elementType == "radioButton") {
            if (this.localSelectedFilter[i].elementName == "verified_interactant") {
              this.localSelectedFilter[i].elementValue1 = this.localSelectedFilter[i].elementValue1 == "0" ? false : true;
              this.localSelectedFilter[i].label = "Verified Interactent";
            } else if (this.localSelectedFilter[i].elementName == "verified_poc") {
              this.localSelectedFilter[i].label = "Verified Poc";
              this.localSelectedFilter[i].elementValue1 = this.localSelectedFilter[i].elementValue1 == "0" ? false : true;
            }
          } else if (this.localSelectedFilter[i].elementType == "text") {
            this.localSelectedFilter[i].label = "Client Name";
            this.localSelectedFilter[i].elementValue1 = this.localSelectedFilter[i].elementValue1 == undefined ? '' : this.localSelectedFilter[i].elementValue1;

          }
          this.filterLookups.forEach(element => {
            element.visible = element.elementName == this.localSelectedFilter[i].elementName ? false : element.visible;
          });
        }

        // console.log(this.availableFields);
        console.log(this.availableFieldsList);
        this.availableFields = res.availableFieldsList;
        this.availableFieldsList = res.availableFieldsList;
        let selectSortorderFields = res.selectSortorderFields == undefined ? [] : res.selectSortorderFields?.split(',');
        let selectedFields = res.selectedFields != undefined ? res.selectedFields?.split(',') : [];
        console.log(selectSortorderFields, selectedFields);
        selectedFields.map(y => {
          this.availableFields.map(x => {
            if (x.field == y) {
              this.selectedFields.push(x);
            }
          })
        })
        this.availableFieldsList = this.availableFieldsList.filter(x => {
          return !selectedFields.includes(x.field)
        })
        this.availableSortorderFields = [];
        for (let i = 0; i < this.selectedFields.length; i++) {
          this.availableSortorderFields.push(this.selectedFields[i]);

        }
        selectSortorderFields.map(y => {
          this.availableSortorderFields.map(x => {
            if (x.field == y) {
              this.selectdSortorderFields.push(x);
            }
          })
        })
        this.availableSortorderFields = this.availableSortorderFields.filter(x => {
          return !selectSortorderFields.includes(x.field)
        })







      })
    } catch (error) {

    }
  }

  public onColumnSelect(event) {
    console.log(event)
    this.availableSortorderFields = [];
    let arr = this.selectdSortorderFields.map(x => {
      return x.id;
    })
    for (let i = 0; i < this.selectedFields.length; i++) {
      if (arr.includes(this.selectedFields[i].id)) {

      } else {
        this.availableSortorderFields.push(this.selectedFields[i]);

      }
    }
  }

  public onColumDeselect(event) {
    console.log(event);
    event.items.map(x => {
      this.selectdSortorderFields = this.selectdSortorderFields.filter(y => {
        return y.id != x.id
      }
      )
      this.availableSortorderFields = this.availableSortorderFields.filter(y => {
        return y.id != x.id
      })
    })

  }
  public genarate() {

    if (this.inquiryStartDate == null || this.inquiryEndDate == null) {
      Swal.fire('', 'Inquiry Start and End Dates are mandatory fields', 'warning');
    } else {
      let startDate = Date.parse(this.datePipe.transform(this.inquiryStartDate, 'MM/dd/yyyy'));
      let endDate = Date.parse(this.datePipe.transform(this.inquiryEndDate, 'MM/dd/yyyy'));
      if (startDate > endDate) {
        Swal.fire('', 'Inquiry Start date cannot be greater than Inquiry End date', 'warning');
      }
      else if (this.reportName == null) {
        Swal.fire('', 'Report Name is mandatory field', 'warning');
      }
      else if (this.reportName.trim().length == 0) {
        Swal.fire('', 'Report Name is mandatory field', 'warning');
      } else if (this.selectedFields.length == 0) {
        Swal.fire('', 'Please select at least one Column', 'warning');
      }
      else {
        this.saveAdhocQueryDetails();
      }
    }
  }


  // this resize() method is used for UI adjustments
  private resize() {
    setTimeout(() => {
      var inputWidth = $("#test").width();
      console.log(inputWidth)
      $('.p-multiselect').css('width', inputWidth + 23.559 + 'px');
    }, 300)
  }



  public branchLookup = [];
  public fundingCategoryLookup = [];
  public recordOwnerLookup = [];

  public slectedFilters = [];
  public onMenuItem(event) {
    console.log(event)

    for (let i = 0; i < this.filterLookups.length; i++) {
      if (this.filterLookups[i].elementName == event.elementName) {
        this.filterLookups[i].visible = false;
        if (event) {
          if (event.elementType == 'selectbox') {
            let obj = {

              label: event.label,
              elementType: event.elementType,
              elementName: event.elementName,
              elementValue1: [],
              isBlank: 0,
              isNotBlank: 0,
              // options: event.label == 'Branch' ? this.branchLookup : event.label == 'Funding Category' ? this.fundingCategoryLookup : event.label == 'Record Owner' ?this.recordOwnerLookup : this._apiservice.inquiriesLookUp
            }
            this.slectedFilters.push(obj);

          } else if (event.elementType == 'dateType') {
            let obj = {
              label: event.label,
              elementType: event.elementType,
              elementName: event.elementName,
              elementValue1: null,
              elementValue2: null,
              selectedValue: null,
              value: null,
              isBlank: 0,
              isNotBlank: 0,
            }
            this.slectedFilters.push(obj);

          } else if (event.elementType == 'text') {
            let obj = {

              label: event.label,
              elementType: event.elementType,
              elementName: event.elementName,
              elementValue1: '',
              isBlank: 0,
              isNotBlank: 0,
            }
            this.slectedFilters.push(obj);

          } else if (event.elementType == 'radioButton') {
            let obj = {

              label: event.label,
              elementType: event.elementType,
              elementName: event.elementName,
              elementValue1: false,
            }
            this.slectedFilters.push(obj);

          }


        }

      }

    }
  }

  public onDeselectFilter(item) {
    this.slectedFilters = this.slectedFilters.filter(x => x.elementName != item.elementName);
    this.filterLookups.forEach(element => {
      element.visible = element.elementName == item.elementName ? true : element.visible;
    });
  }
  public onDateUpdate(i, op) {
    if (this.slectedFilters[i].selectedValue != null) {
      if (this.slectedFilters[i].selectedValue == 'between') {
        if (this.slectedFilters[i].elementValue1 == null || this.slectedFilters[i].elementValue2 == null) {
          Swal.fire('', 'Between Dates cannot be empty', 'warning')
        } else {
          let start=Date.parse(this.datePipe.transform(this.slectedFilters[i].elementValue1, 'MM/dd/yyyy'));
          let end=Date.parse(this.datePipe.transform(this.slectedFilters[i].elementValue2, 'MM/dd/yyyy'));
          if(start>end){
            Swal.fire('', 'Start date   cannot be greater than End date', 'warning')
          }else{
          this.slectedFilters[i].value = this.datePipe.transform(this.slectedFilters[i].elementValue1, 'MM/dd/yyyy') + '-' + this.datePipe.transform(this.slectedFilters[i].elementValue2, 'MM/dd/yyyy');
          this.slectedFilters[i].isBlank = this.slectedFilters[i].selectedValue == "Is Blank" ? 1 : 0;
          this.slectedFilters[i].isNotBlank = this.slectedFilters[i].selectedValue == "Is Not Blank" ? 1 : 0;
          op.hide();
          }
        }
      } else {
        this.slectedFilters[i].elementValue1 = null;
        this.slectedFilters[i].elementValue2 = null;
        this.slectedFilters[i].value = this.slectedFilters[i].selectedValue;
        this.slectedFilters[i].isBlank = this.slectedFilters[i].selectedValue == "Is Blank" ? 1 : 0;
        this.slectedFilters[i].isNotBlank = this.slectedFilters[i].selectedValue == "Is Not Blank" ? 1 : 0;
        op.hide();

      }

    } else {
      Swal.fire('', 'Between Dates cannot be empty', 'warning')

    }
  }

  public multiSelectChange(index, elementName) {
    if (elementName == "branch") {
      // console.log("2########$$$$$$%%%%%%")
      this.branchLookup = this.sortArrayBySelected(this.branchLookup, this.slectedFilters[index].elementValue1)
    } else if (elementName == "funding_category") {
      this.fundingCategoryLookup = this.sortArrayBySelected(this.fundingCategoryLookup, this.slectedFilters[index].elementValue1);
    } else if (elementName == "record_owner") {
      this.recordOwnerLookup = this.sortArrayBySelected(this.recordOwnerLookup, this.slectedFilters[index].elementValue1);
    }
  }
  // this method is used to display selected option to the top for multi select
  private sortArrayBySelected(items: any = [], selected: any = []) {
    // console.log("3########$$$$$$%%%%%%")
    // console.log(items, selected)
    items.sort((a, b) => {
      let findAIndex = selected.findIndex((i => { if (a['id'] < 0) { return 1 } else { return i == a['id'] } }));
      let findBIndex = selected.findIndex((i => { if (b['id'] < 0) { return 1 } else { return i == b['id'] } }));
      if (findAIndex > -1 && findBIndex == -1)
        return -1;
      else if (findAIndex == -1 && findBIndex > -1)
        return 1;

      return 0;
    });
    return items;
  }

  public getLookupsData() {
    try {
      this._apiservice.getlookupdateOptional('users,branch,funding_category').subscribe(res => {
        this.branchLookup = res?.branch;
        this.fundingCategoryLookup = res.funding_category;
        this.recordOwnerLookup = res.users;
        let isBlankObj = { id: "isBlank", name: 'Is Blank' };
        let isNotBlankObj = { id: "isNotBlank", name: 'Is Not Blank' };
        this.branchLookup.splice(0, 0, isNotBlankObj);
        this.branchLookup.splice(0, 0, isBlankObj);
        this.fundingCategoryLookup.splice(0, 0, isNotBlankObj);
        this.fundingCategoryLookup.splice(0, 0, isBlankObj);

      })
    } catch (error) {

    }
  }

  public onSetFilter() {
    console.log("onSEt method")
    let errorFlag = false;
    for (let i = 0; i < this.slectedFilters.length; i++) {
      if (this.slectedFilters[i].elementType == 'selectbox') {

        if (this.slectedFilters[i].elementValue1.length == 0) {
          errorFlag = true;
          Swal.fire('', this.slectedFilters[i].label + ' field cannot be empty', 'warning');
          break;
        }

      } else if (this.slectedFilters[i].elementType == 'dateType') {
        if (this.slectedFilters[i].isBlank == 0 && this.slectedFilters[i].isNotBlank == 0) {
          if (this.slectedFilters[i].elementValue1 == null || this.slectedFilters[i].elementValue2 == null) {
            errorFlag = true;
            Swal.fire('', this.slectedFilters[i].label + ' field cannot be empty', 'warning');
            break;
          }
        }
      }

    }
    if (!errorFlag) {
      this.displayResponsive = false;
      console.log(this.slectedFilters[0]===this.localSelectedFilter[0]);

      this.localSelectedFilter=[];
      for(let i=0;i<this.slectedFilters.length;i++){

        if(this.slectedFilters[i].elementType=='selectbox'){
          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            isBlank: this.slectedFilters[i].elementValue1.includes('isBlank') ? 1 : 0,
            isNotBlank: this.slectedFilters[i].elementValue1.includes('isNotBlank') ? 1 : 0,
            elementValue1: this.slectedFilters[i].elementValue1==undefined?'':this.slectedFilters[i].elementValue1,
            elementValue2: this.slectedFilters[i].elementValue2==undefined?'':this.slectedFilters[i].elementValue2,
            label:this.slectedFilters[i].label,

          }
          this.localSelectedFilter.push(obj);

        }else if(this.slectedFilters[i].elementType=='dateType'){

          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            elementValue1:this.slectedFilters[i].elementValue1,
            elementValue2:this.slectedFilters[i].elementValue2,
            isBlank: this.slectedFilters[i].isBlank,
            isNotBlank: this.slectedFilters[i].isNotBlank,
            label:this.slectedFilters[i].label,

          }
          this.localSelectedFilter.push(obj);
        }else if (this.slectedFilters[i].elementType == 'text') {
          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            elementValue1: this.slectedFilters[i].elementValue1??='',
            elementValue2: '',
            isBlank: 0,
            isNotBlank: 0,
            label:this.slectedFilters[i].label

          }
          this.localSelectedFilter.push(obj);
        } else if (this.slectedFilters[i].elementType == 'radioButton') {
          let obj = {
            elementType: this.slectedFilters[i].elementType,
            elementName: this.slectedFilters[i].elementName,
            elementValue1: this.slectedFilters[i].elementValue1 == false ? 0 : 1,
            elementValue2: '',
            isBlank: 0,
            isNotBlank: 0,
            label:this.slectedFilters[i].label
          }
          this.localSelectedFilter.push(obj);
        }

      }
      console.log(this.slectedFilters===this.localSelectedFilter);
      console.log(this.slectedFilters[0]===this.localSelectedFilter[0]);
      console.log(this.slectedFilters,this.localSelectedFilter)


      // for (let i = 0; i < this.slectedFilters.length; i++) {
      //   this.localSelectedFilter.push(this.slectedFilters[i]);

      // }
    } else {
      this.displayResponsive = true;
    }
  }
  public displayResponsive: boolean = false;
  public openAdvanceFilter() {
    this.slectedFilters=[];
    this.displayResponsive = true;
    // this.slectedFilters =[...this.localSelectedFilter];

    for(let i=0;i<this.localSelectedFilter.length;i++){
      if(this.localSelectedFilter[i].elementType=='selectbox'){
        let obj = {
          elementType: this.localSelectedFilter[i].elementType,
          elementName: this.localSelectedFilter[i].elementName,
          isBlank: this.localSelectedFilter[i].elementValue1.includes('isBlank') ? 1 : 0,
          isNotBlank: this.localSelectedFilter[i].elementValue1.includes('isNotBlank') ? 1 : 0,
          elementValue1: this.localSelectedFilter[i].elementValue1==undefined?'':this.localSelectedFilter[i].elementValue1,
          elementValue2: this.localSelectedFilter[i].elementValue2==undefined?'':this.localSelectedFilter[i].elementValue2,
          label:this.localSelectedFilter[i].label,

        }
        this.slectedFilters.push(obj);

      }else if(this.localSelectedFilter[i].elementType=='dateType'){

        let obj = {
          elementType: this.localSelectedFilter[i].elementType,
          elementName: this.localSelectedFilter[i].elementName,
          elementValue1:this.localSelectedFilter[i].elementValue1,
          elementValue2:this.localSelectedFilter[i].elementValue2,
          isBlank: this.localSelectedFilter[i].isBlank,
          isNotBlank: this.localSelectedFilter[i].isNotBlank,
          label:this.localSelectedFilter[i].label,

        }
        this.slectedFilters.push(obj);
      }else if (this.localSelectedFilter[i].elementType == 'text') {
        let obj = {
          elementType: this.localSelectedFilter[i].elementType,
          elementName: this.localSelectedFilter[i].elementName,
          elementValue1: this.localSelectedFilter[i].elementValue1??='',
          elementValue2: '',
          isBlank: 0,
          isNotBlank: 0,
          label:this.localSelectedFilter[i].label

        }
        this.slectedFilters.push(obj);
      } else if (this.localSelectedFilter[i].elementType == 'radioButton') {
        let obj = {
          elementType: this.localSelectedFilter[i].elementType,
          elementName: this.localSelectedFilter[i].elementName,
          elementValue1: this.localSelectedFilter[i].elementValue1 == false ? 0 : 1,
          elementValue2: '',
          isBlank: 0,
          isNotBlank: 0,
          label:this.localSelectedFilter[i].label

        }
        this.slectedFilters.push(obj);
      }

    }

    console.log(this.slectedFilters[0]===this.localSelectedFilter[0]);
    this.filterLookups.forEach(element => {
      element.visible = true;
    });
    for (let i = 0; i < this.slectedFilters.length; i++) {
      if (this.slectedFilters[i].elementType == "dateType" && this.slectedFilters[i].elementName != 'inquiry_date') {
        if (this.slectedFilters[i].isBlank == "0" && this.slectedFilters[i].isNotBlank == "0") {
          this.slectedFilters[i].value = this.datePipe.transform(this.slectedFilters[i].elementValue1, 'MM/dd/yyyy') + '-' + this.datePipe.transform(this.slectedFilters[i].elementValue2, 'MM/dd/yyyy');
          this.slectedFilters[i].selectedValue = 'between';
        } else if (this.slectedFilters[i].isBlank == "1") {
          this.slectedFilters[i].value = 'Is Blank';
          this.slectedFilters[i].selectedValue = 'Is Blank';
        } else if (this.slectedFilters[i].isNotBlank == "1") {
          this.slectedFilters[i].value = 'Is Not Blank';
          this.slectedFilters[i].selectedValue = 'Is Not Blank';
        }
      }
      if (this.slectedFilters[i].elementName != 'inquiry_id' && this.slectedFilters[i].elementType == 'selectbox') {
        console.log("1########$$$$$$%%%%%%")
        this.multiSelectChange(i, this.slectedFilters[i].elementName)
      }
      this.filterLookups.forEach(element => {
        element.visible = (element.elementName == this.slectedFilters[i].elementName) ? false : element.visible;
      });
    }
    console.log(this.filterLookups)


  }

  public overlayPanel=false;
}
