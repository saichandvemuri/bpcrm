import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/service/api.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-saved-queries',
  templateUrl: './saved-queries.component.html',
  styleUrls: ['./saved-queries.component.scss']
})
export class SavedQueriesComponent implements OnInit {
  public savedAdHocQueryList = [];
  constructor(public _apiservice: ApiService,private router:Router) {
    this._apiservice.SetheaderName('Saved Queries');
    sessionStorage.removeItem('reportId');

  }

  ngOnInit(): void {
    this.getSavedAdHocQueryList();
  }

  public getSavedAdHocQueryList() {
    try {
      this._apiservice.getSavedAdHocQueryList().subscribe(res => {
        console.log(res);
        this.savedAdHocQueryList = res.savedAdHocQueryList;
      })
    } catch (error) {

    }
  }

  public deleteAdhocQuery(id){

    Swal.fire({
      title: 'Are you sure you want to Delete?',
      // text: "you want to Logout?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete!'
    }).then((result) => {
      if (result.isConfirmed) {
        try {
          this._apiservice.deleteAdhocQuery(id).subscribe(res=>{
            Swal.fire('',res.message,'success');
            this.getSavedAdHocQueryList();
          })
        } catch (error) {

        }
      }
    })




  }
  public onSavedQueries(id,name){
    sessionStorage.setItem('reportId',id);
    sessionStorage.setItem('fileName',name);
    this.router.navigateByUrl('/saved-events')

  }
  public onEditQuery(id){
    sessionStorage.setItem('reportId',id);
    this.router.navigateByUrl('/create-query')

  }

}
